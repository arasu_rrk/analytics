# Capacity :weight_lifter: 

## Time :calendar: 
Please add a number for days you'll be working this milestone. Engineers should reserve a day for each triage day and a day for meetings.


- @jjstark: 
- @msendal: 
- @m_walker: 
- @tayloramurphy: 

**Total Engineering Days**: 

## Velocity :race_car: 

Last Milestone: <!-- link here -->
* **Total weight completed:** 
* **Capacity:** 
* **Velocity:** 


## Weight Available: :crystal_ball: 
Current Milestone:
* **Eng Days:** 
* **Velocity:** 

**Engineering capacity for this milestone:**

---

# Prioritized Issues :8ball: 

This section is for adding issues that have been prioritized by either the data team or function groups. Prioritization should be discussed in the comments. 

## OKRs :dart: 
1. <!-- link here --> 

## Function Groups :two_women_holding_hands: 
1. <!-- link here -->

---
<!-- DO NOT EDIT BELOW THIS LINE -->
/label ~Housekeeping ~Infrastructure