version: 2

models:
  - name: employee_directory_analysis
    description: Gives the current state of the employees at GitLab at any point of time. This is the model to use for headcount, team size, or any people-related analysis for employees. This has current and past employees, as well as their department, division, and cost center and hire/termination dates.
    columns:
    - name: unique_key
      tests:
      - not_null
      - unique
    - name: date_actual
      tests:
      - not_null
    - name: full_name
      tests:
      - not_null
    - name: job_title
    - name: department
    - name: division
    - name: location_factor
    - name: is_hire_date
      tests:
      - not_null
    - name: is_termination_date
      tests:
      - not_null
    - name: layers
      description: '{{ doc("employee_layers") }}'
      tests:
      - not_null
  - name: employee_directory_intermediate
    description: INCLUDES SENSITIVE INFORMATION. The master collection of all info about GitLab employees for their latest position.
    columns:
    - name: employee_id
      tests:
      - not_null
    - name: employee_number
      tests:
      - not_null
    - name: first_name
      tests:
      - not_null
    - name: last_name
      tests:
      - not_null
    - name: job_title
    - name: supervisor
    - name: work_email
    - name: hire_date
      tests:
       - not_null
    - name: termination_date
    - name: department
    - name: division
    - name: cost_center
    - name: location_factor
    - name: layers
      description: '{{ doc("employee_layers") }}'
      tests:
      - not_null
  - name: bamboohr_discretionary_bonuses
    description: This model contains a list of Discretionary bonuses awarded (excludes other types of bonuses). It includes the employee ID to be able to join to the `employee_directory_analysis` model to understand the break down of discretionary bonuses given.
    columns:
    - name: bonus_id
      tests:
        - not_null
        - unique
    - name: employee_id
      tests:
        - not_null
    - name: bonus_date
      tests:
        - not_null
  - name: bamboohr_employment_status_xf
    description: This model provides a transaction record of an employee's status changes (i.e. active, leave, termed). It helps identify when an employee is re-hired.
    columns:
        - name: employee_id
          tests:
            - not_null
        - name: employment_status
          tests:
            - not_null
        - name: is_rehire
          description: Identifies re-hired employees based on having a previous employment status of "Terminated."
          tests:
            - not_null
            - accepted_values:
                    values: ['True', 'False']
        - name: termination_type
          description: For an employment status record equal to "Terminated" the termination_type will identify if it was voluntary or involuntary.
        - name: next_employment_status
          description: Captures the next employment status if one exists. In the case there is none the next_employment_status will be null.
        - name: valid_from_date
          description: Provides the start date for the specific employment status record.
          tests:
            - not_null
        - name: valid_to_date
          description: Provides the end date for a specific record. For the terminated record stamp, it will provide a null since there will be no following record, unless the employee is re-hired, in which case it will indicate how long the employee was separated.
  - name: bamboohr_headcount_aggregation_intermediate
    description: This is the intermediate model to creating out the headcount report. This report provides a count breakdown of headcounts, hires, separations by diversity fields.
    columns:
        - name: gender
          tests:
            - not_null
        - name: ethnicity
          tests:
            - not_null
        - name: nationality
          tests:
            - not_null
        - name: region
          tests:
            - not_null
        - name: total_count
          description: Provides the count of employees that tie to the applicable metric. For example, total_count tied to the metric headcount_start shows how many employees were active on the 1st of the month.
          tests:
            - not_null
        - name: metric
          description: Indicates the metric being created.
          tests:
            - accepted_values:
                values: ['headcount_start', 'headcount_end', 'hires', 'total_separated', 'voluntary_separations', 'involuntary_separations']
